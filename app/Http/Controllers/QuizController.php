<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Word;
use App\Definition;
use Illuminate\Support\Arr;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $words = Word::paginate(1);
        foreach($words as $loopWord){
            $word = Word::find($loopWord->id);
            $question = 'What is ' . $word->word . '?';

            foreach($word->definitions as $loopDefinition){
                $definition = Definition::where('id' , '!=' , $loopDefinition->id)->get();
                $data['first'] = array($loopDefinition->definition);
                foreach($definition as $answer){
                    $data['result'] = Arr::random(Arr::pluck($definition, 'definition'), 3);
                }
                $data = array(Arr::flatten($data));
            }
        }
        return view('userViews/mocktest', ['categories' => $categories, 'questions' => $question, 'answer' => $data, 'page' => $words]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function category($category)
    {
        $allCategories = Category::all();
        $categories = Category::where('name', 'LIKE', '%' . $category . '%')->get();
        $data = [];
        foreach($categories as $loopCategory){
            $findWord = Category::find($loopCategory->id)->words()->paginate(1);
            foreach($findWord as $loopWord){
                $word = Word::find($loopWord->id);
                
                
                $question = 'What is ' . $word->word . '?';
                foreach($word->definitions as $loopDefinition){
                    $definition = Definition::where('id' , '!=' , $loopDefinition->id)->get();
                    $data['first'] = array($loopDefinition->definition);
                    foreach($definition as $answer){
                        $data['result'] = Arr::random(Arr::pluck($definition, 'definition'), 3);
                    }
                    $data = array(Arr::flatten($data));
                }
            }
        }

        return view('userViews/mocktest', ['categories' => $allCategories, 'questions' => $question, 'answer' => $data,'page' => $findWord]);
    }
}
