<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Word;

class WordsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::all();
        $words = Word::paginate(1);
        foreach($words as $loopWord){
            $word = Word::find($loopWord->id);
            foreach($word->definitions as $loopDefinition){
                $data[] = array("word" => $word->word, "definition" => $loopDefinition->definition, "category" => $word->category->name);
            }
        }
        return view('userViews/NewWords', ['categories' => $categories, 'words' => $data, 'page' => $words, 'image' => $word->image_path]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function category($category)
    {
        $allCategories = Category::all();
        $categories = Category::where('name', 'LIKE', '%' . $category . '%')->get();
        $data = [];
        foreach($categories as $loopCategory){
            $findWord = Category::find($loopCategory->id)->words()->paginate(1);
            foreach($findWord as $loopWord){
                $word = Word::find($loopWord->id);
                foreach($word->definitions as $loopDefinition){
                    $data[] = array("word" => $word->word, "definition" => $loopDefinition->definition, "category" => $word->category->name);      
                }
            }
        }

        return view('userViews/NewWords', ['categories' => $allCategories, 'words' => $data, 'page' => $findWord]);
    }
}