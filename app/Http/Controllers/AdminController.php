<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Word;
use App\Definition;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
        $categories = Category::all();
        $total = Word::all()->count();
        return view('adminViews/dashboard_page', ['categories' => $categories, 'total' => $total]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function totalWord()
    {
        // 
        $words = Word::paginate(5);
        foreach($words as $loopWord){
            $word = Word::find($loopWord->id);
            
            foreach($word->definitions as $loopDefinition){
                $data[] = array("wordid" => $word->id, "word" => $word->word, "definitionid" => $loopDefinition->id, "definition" => $loopDefinition->definition, "category" => $word->category->name);
                
            }
        }

        return view('adminViews/total_words_page', ['words' => $data, 'page' => $words]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchTotalWords(Request $request)
    {
        $keyword = $request->input('keyword');
        $words = Word::where('word', 'LIKE', '%' . $keyword . '%')
        ->paginate(5);

        foreach($words as $loopWord){
            $word = Word::find($loopWord->id);
            
            foreach($word->definitions as $loopDefinition){
                $data[] = array("wordid" => $word->id, "word" => $word->word, "definitionid" => $loopDefinition->id, "definition" => $loopDefinition->definition, "category" => $word->category->name);
                
            }
        }

        return view('adminViews/total_words_page', ['words' => $data, 'page' => $words]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createNewWord(Request $request)
    {
        
        $this->validate($request, [
            'word' => 'required',
            'definition' => 'required',
            'selectCategory' => 'required'
        ]);
        
        $inputword = $request->input('word');
        $words = Word::where('word', $inputword)->get();

        if(count($words) > 0){
            foreach($words as $word){

                $word->word = $request->input('word');
                $word->category_id = $request->input('selectCategory');
                if($request->hasFile('image')) {
                    $request->validate([
                        'image' => 'file|image|max:5000'
                    ]);
                    $images = $request->image;
                    $word->image_path = $images->store('images', 'public');
                }
                $word->save();

                foreach($word->definitions()->get() as $definition){
                    $definition->definition = $request->input('definition');
                    $definition->save();
                }
                
            }
        }
        else{
            $word = new Word;
            $word->word = $request->input('word');
            $word->category_id = $request->input('selectCategory');
            if($request->hasFile('image')) {
                $request->validate([
                    'image' => 'file|image|max:5000'
                ]);
                $images = $request->image;
                $word->image_path = $images->store('images', 'public');
            }
            $word->save();

            $definition = new Definition;
            $definition->definition = $request->input('definition');
            $definition->save();

            $word->definitions()->attach($definition);
        }
        
        return redirect('totalword');
    }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function createNewCategory(Request $request)
        {
            //
            $this->validate($request, [
                'category' => 'required'
            ]);
            
            $categories = Category::where('name', $request->input('category'))->get();
            
            if(count($categories) > 0){
                foreach($categories as $category){
                    $category->name = $request->input('category');
                    $category->save();
                }
            }
            else
            {
                $category = new Category;
                $category->name = $request->input('category');
                $category->save();
            }
            

            return redirect('totalword');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function deleteWordandDefinition($wordid, $definitionid)
        {
            //
            Word::find($wordid)->definitions()->detach(Definition::find($definitionid));
            Word::destroy($wordid);
            Definition::destroy($definitionid);
            
            return redirect('totalword');
        }
    }