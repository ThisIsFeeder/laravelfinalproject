<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Definition extends Model
{
    //
    public function words(){
        return $this->belongsToMany(Word::class)->withTimestamps();
    }
}
