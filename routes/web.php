<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// UserRoute
// HomepageRoute
Route::get('/', function () {
    return view('userViews/homepage');
});

// LearningNewWordsRoute
Route::get('/word', 'WordsController@index')->name('words');

// LearningNewWordsByCategoryRoute
Route::get('/word/{category}', 'WordsController@category')->name('word');

// MockTestRoute
Route::get('/quiz', 'QuizController@index')->name('quiz');

// LearningQuizByCategoryRoute
Route::get('/quiz/{category}', 'QuizController@category')->name('bycategory');


// AdminRoute
//HomepageRout
Route::get('/home', 'AdminController@index')->name('dashboard');

//TotalwordRoute
Route::get('/totalword', 'AdminController@totalWord');

//CreateNewWordRoute
Route::post('/createnewword', 'AdminController@createNewWord');

//CreateNewCategoryRoute
Route::post('/createnewcategory', 'AdminController@createNewCategory');

//DeleteWord
Route::get('/word/{wordid}/{definitionid}', 'AdminController@deleteWordandDefinition')->name('destroy');

//SearchWordRoute
Route::get('totalword', 'AdminController@searchTotalWords');


//LoginRoute
Auth::routes();

