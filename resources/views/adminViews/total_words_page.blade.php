@extends ('layouts.mainadminlayout')

@section('title','Total Words')
@section('leftcard')

@endsection


@section('contentview')
<div class="tablecontent">
    <br>
    <h4>Total Words</h4>
    <form action="totalword" method="GET" class="form-inline">
        <input name="keyword" class="custom-select" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    <div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Words</th>
                    <th scope="col">Type</th>
                    <th scope="col">Description</th>
                    <th scope="col">Delete</th>
                </tr>
            </thead>
            <tbody>
                @for ($index = 0; $index < count($words); $index++) <tr>
                    <td>{{$words[$index]['word']}}</td>
                    <td>{{$words[$index]['category']}}</td>
                    <td>{{$words[$index]['definition']}}</td>
                    <td>
                        <button type="button" class="btn btn-outline-danger" onclick="window.location = '{{ route('destroy', [$words[$index]['wordid'], $words[$index]['definitionid']])}}'">Delete</button>
                    </td>
                    </tr>
                    @endfor
            </tbody>
        </table>
    </div>
    <div class="customized-pagination">
        {{ $page->links() }}
    </div>
    @endsection