<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
</head>

<body>
    <!-- addwords  -->
    <div id="modaladdwords" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title">Add Words</h1>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/createnewword') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <!-- AddImage -->
                        <label for="img" class="control-label">Image</label>
                        <div class="form-group">
                            <div class="box">
                                <div class="js--image-preview"></div>
                                <div class="upload-options">
                                    <input type="file" class="image-upload py-2" name="image"/>
                                </div>

                                <!-- AddContent -->
                                <label for="words_text" class="control-label">Words</label>
                                <div>
                                    <input id="words" type="text" class="form-control" placeholder='Word' name="word"
                                        autofocus>
                                </div>
                                <label for="category_text" class="control-label">Category</label>
                                <div>
                                    <select class="custom-select" id="inputGroupSelect02" name='selectCategory' />
                                    @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <label for="definition" class="control-label">Definition</label>
                                <div>
                                    <textarea rows="4" placeholder="Definition" class="form-control" name="definition"
                                        autofocus></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-primary" style="float:right;">
                                    Add
                                </button>
                                <a class="btn btn-link">
                                    Result
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</body>

</html>