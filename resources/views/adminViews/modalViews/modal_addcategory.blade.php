<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
</head>

<body>

    <!-- addwords  -->
    <div id="modaladdcategory" class="modal fade">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title">Add Category</h1>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/createnewcategory') }}">
                        {{ csrf_field() }}
                        <!-- AddContent -->
                        <label for="category" class="control-label">Category</label>
                        <div>
                            <input type="text" class="form-control" name="category" autofocus>
                        </div>
                        <div class="form-group">
                            <br>
                            <div>
                                <button type="submit" class="btn btn-primary" style="float:right;">
                                    Add
                                </button>
                                <a class="btn btn-link">
                                    Result
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</body>

</html>