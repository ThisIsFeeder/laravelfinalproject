@extends ('layouts.mainadminlayout')
@include ('adminViews/modalViews/modal_addwords')
@include ('adminViews/modalViews/modal_addcategory')

@section('title','DashBoard')

@section('leftcard')
<div class="container">
    <div class="row">
        <div class="col">
            <h4>Configuration</h4>
            <div class="card" style="max-width: 18rem;">
                <div class="card-body" data-toggle="modal" data-target="#modaladdwords" class="title m-b-md">
                    <h5 class="card-title">Add New<br>Word</h5>
                </div>
            </div>
            <div class="card" style="max-width: 18rem;">
                <div class="card-body" data-toggle="modal" data-target="#modaladdcategory" class="title m-b-md">
                    <h5 class="card-title">Add New<br>Category</h5>
                </div>
            </div>

        </div>
        <div class="col">
            <h4>Summary</h4>
            <div class="card" style="max-width: 26rem; height:220px;">
                <div class="card-body">
                    <a class="nav-link p-0" href="{{ url('/totalword') }}">
                        <h5 class="card-title">Total<br>Words</h5>
                        <p class="card-text">{{$total}}</p>

                </div>
            </div>
        </div>
    </div>

    @endsection