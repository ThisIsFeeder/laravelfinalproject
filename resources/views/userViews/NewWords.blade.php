@extends('layouts.mainuserlayout')

@section('title','New Word')

@section('content')

<!-- Banner Starts -->

<!-- Banner Ends -->

<!--Extended Content Starts-->
<div class="content">

    <!--Categories Bar Starts-->
    <div class="cat-bar">
        <div class="cat-bar-title">
            <h4>Word Categories</h4>
        </div>
        <div class="cat-list">
            <ul class="categories">
                @foreach($categories as $category)
                <li class="cat-item">
                    <a href="{{route('word', $category->name)}}" class="cat-link">{{$category->name}}</a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <!--Categories Bar Ends-->

    <!-- Word Card Starts -->
    <div class="word-card">

        <!-- Content Body Starts -->
        <div class="content-body">
            @if(count($page) > 0)
            <!-- Word Title Starts -->
            <div class="word-title">
                @for ($index = 0; $index < count($words); $index++) <tr>
                    <h2 class="word">{{$words[$index]['word']}}</h2><br>
                    @endfor
            </div>
            <!-- Word Title Ends -->

            <!-- Content Row Starts -->
            <div class="content-row">

                <!-- Left Side Content Starts -->
                <div class="word-card-content-left">

                    <!-- Word Definition Starts -->
                    <div class="word-definition">
                        <article id="article">
                            @for ($index = 0; $index < count($words); $index++) <tr>
                                {{$words[$index]['definition']}}
                                @endfor
                        </article>
                    </div>
                    <!-- Word Definition Ends -->
                    <!-- Pagination Starts -->
                    <div class="customized-pagination">
                        {{ $page->links('vendor.pagination.default') }}
                    </div>
                    <!-- Pagination Ends -->

                </div>
                <!-- Left Side Content End -->

                <!-- Right Side Content Starts -->
                <div class="word-card-content-right">
                    @if($image)
                    <img src="{{ asset('storage/' . $image) }}" alt="image here">
                    @else
                    <img src="/drawble/banner1.png" alt="image here">
                    @endif
                </div>
                <!-- Right Side Content Ends -->

            </div>
            <!-- Content Row Ends -->
            @else
            <!-- Word Title Starts -->
            <div class="word-title">
                <h2 class="word">No Word</h2><br>
            </div>
            <!-- Word Title Ends -->

            <!-- Content Row Starts -->
            <div class="content-row">

                <!-- Left Side Content Starts -->
                <div class="word-card-content-left">

                    <!-- Word Definition Starts -->
                    <div class="word-definition">
                        <article id="article">
                            There ain't a word for IT.
                        </article>
                    </div>
                    <!-- Word Definition Ends -->
                    <!-- Pagination Starts -->
                    <div class="customized-pagination">
                        {{ $page->links('vendor.pagination.default') }}
                    </div>
                    <!-- Pagination Ends -->

                </div>
                <!-- Left Side Content End -->

                <!-- Right Side Content Starts -->
                <div class="word-card-content-right">
                    <img src="/drawble/banner1.png" alt="image here">
                </div>
                <!-- Right Side Content Ends -->

            </div>
            <!-- Content Row Ends -->
            @endif
        </div>
        <!-- Content Body Ends -->

    </div>
    <!-- Word Card Ends -->

</div>

@endsection