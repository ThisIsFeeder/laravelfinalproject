@extends('layouts.mainuserlayout')
@section('title','Homepage')
@section('content')
<!-- Banner Starts -->
<div class="banner">
    <img class="a" src="{{url('/drawble/books.png')}}" alt="Image">
    <img class="b" src="{{url('/drawble/writing1.png')}}" alt="Image">
    <div class="text-container">
        <div class="side-a">
            <span class="leading-title">
                Learn
            </span>
            <br>
            <span class="subtitle">
                All new vocabularies with a variety of organized categories of words.
            </span>
            <button class="btn" id="btn-new-word" onclick="window.location = '{{ route('words') }}'">Let's Go</button>
        </div>
        <div class="side-b">
            <span class="leading-title">
                Test
            </span>
            <br>
            <span class="subtitle">
            The capacity of your knowledge and see how wide it could be.
            </span>
            <button class="btn" id="btn-mock-test" onclick="window.location = '{{ route('quiz') }}'">Try It</button>
        </div>
    </div>
</div>
<!-- Banner Ends -->

@endsection