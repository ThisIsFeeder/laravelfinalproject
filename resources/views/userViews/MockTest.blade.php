@extends('layouts.mainuserlayout')

@section('title','Quiz')

@section('content')

<!-- Banner Starts -->

<!-- Banner Ends -->

<!--Extended Content Starts-->
<div class="content">

    <!--Categories Bar Starts-->
    <div class="cat-bar">
        <div class="cat-bar-title">
            <h4>Word Categories</h4>
        </div>
        <div class="cat-list">
            <ul class="categories">
                @foreach($categories as $category)
                <li class="cat-item">
                    <a href="{{route('bycategory', $category->name)}}" class="cat-link">{{$category->name}}</a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <!--Categories Bar Ends-->

    <!-- Word Card Starts -->
    <div class="word-card">

        <!-- Content Body Starts -->
        <div class="content-body">
            
            @if(count($page) > 0)
            <!-- Content Row Starts -->
            <div class="content-row">

                <!-- Left Side Content Starts -->
                <div class="word-card-content-left">

                    <!-- Left Side Label Starts -->
                    <div class="side-label">
                        <h5>
                            Question
                        </h5>
                    </div>
                    <!-- Left Side Label Ends -->

                    <!-- Word Definition Starts -->
                    <div class="word-definition">
                        <article id="article">
                            {{$questions}}
                        </article>
                    </div>
                    <!-- Word Definition Ends -->

                    <!-- Pagination Starts -->
                    <div class="customized-pagination">
                        {{ $page->links('vendor.pagination.default') }}
                    </div>
                    <!-- Pagination Ends -->

                </div>
                <!-- Left Side Content End -->

                <!-- Right Side Content Starts -->
                <div class="word-card-content-right">

                    <!-- Left Side Label Starts -->
                    <div class="side-label">
                        <h5>
                            Options
                        </h5>
                    </div>
                    <!-- Left Side Label Ends -->

                    <!-- Options Container Starts -->
                    <div class="options-container">
                        <ul class="options">
                            <li class="option-item">
                                <a href="" class="option-link">A. Phnom Penh</a>
                            </li>
                            <li class="option-item">
                                <a href="" class="option-link">B. Tokyo</a>
                            </li>
                            <li class="option-item">
                                <a href="" class="option-link">C. New York</a>
                            </li>
                            <li class="option-item">
                                <a href="" class="option-link">D. London</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Options Container Starts -->

                </div>
                <!-- Right Side Content Ends -->

            </div>
            <!-- Content Row Ends -->
            @endif
        </div>
        <!-- Content Body Ends -->

    </div>
    <!-- Word Card Ends -->

</div>

@endsection