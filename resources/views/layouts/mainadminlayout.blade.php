<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Terminology - @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/adminStyle.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css')}}">

</head>

<body>

    <!--Header Start-->
    <div class="header">
        <div class="logo">
            <a href="{{ url('/') }}">
                <span>English</span><br>Terminology
            </a>
        </div>

        <div class="header-right">
            <ul class='navigators'>
                <li class="nav-item dropdown">
                    <div class="dropdown-content" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">Log Out</a>
                    </div>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
    <!--Header Ends-->

    <div class="bottombarr">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="{{route('dashboard')}}">DashBoard</a>
            </div>
        </nav>
    </div>

    <div class="leftfooterr">
        @section('leftcard')
        @show
    </div>

    <div class="content">
        @section('contentview')
        @show
    </div>


</body>

</html>