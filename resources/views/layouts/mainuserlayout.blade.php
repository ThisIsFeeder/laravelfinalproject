<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Terminology - @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/userstyle.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
</head>

<body>
    <div id="app">

        <!--Header Start-->
        <div class="header">
            <div class="logo">
                <a href="{{ url('/') }}">
                    <span>English</span><br>Terminology
                </a>
            </div>

            <div class="header-right">

                <ul class='navigators'>
                    <li class='nav-item'>
                        <a href="{{ route('words') }}" class="nav-link">New Words</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('quiz')}}" class="nav-link">Mock Test</a>
                    </li>
                    <li class="nav-item">
                        @if (Route::has('login'))
                        @auth
                        <a class='nav-link' href="{{ url('/home') }}">Home</a>
                        @else
                        <a class='nav-link' href="{{ route('login') }}">Login</a>
                        @endauth
                        @endif
                    </li>
                </ul>
            </div>
        </div>
        <!--Header Ends-->

        <!--Body Wrapper Starts-->
        <div class="body-wrapper">

            <!-- <div class="content-wrapper">
            </div> -->

            @yield('content')

        </div>
        <!--Body Wrapper Ends-->
    </div>
</body>

</html>