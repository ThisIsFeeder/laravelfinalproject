@if ($paginator->hasPages())
{{-- Previous Page Link --}}
<div class="btn-wrapper">
    @if ($paginator->onFirstPage())
    <button class='btn left d-none' aria-disabled="true" aria-label="@lang('pagination.previous')">
        <span aria-hidden="true"><i class="fas fa-arrow-left"></i>Previous</span>
    </button>
    @else
    <button class='btn left' onclick="window.location = '{{ $paginator->previousPageUrl() }}'" rel="prev"
        aria-label="@lang('pagination.previous')"><i class="fas fa-arrow-left"></i>Previous</button>
    @endif
</div>

{{-- Pagination Elements --}}
<div class="card-count">
    @foreach ($elements as $element)
    <p>
        <span class="current-card-index">
            {{$paginator->currentPage()}}
        </span>
        of
        <span class="card-length">
            {{count($element)}}
        </span>
        words
    </p>
    @endforeach
</div>

{{-- Next Page Link --}}
<div class="btn-wrapper">
    @if ($paginator->hasMorePages())
    <button class='btn right' onclick="window.location = '{{ $paginator->nextPageUrl() }}'" rel="next"
        aria-label="@lang('pagination.next')">Next<i class="fas fa-arrow-right"></i></button>
    @else
    <button class='btn right d-none' aria-disabled="true" aria-label="@lang('pagination.next')">
        <span aria-hidden="true">Next<i class="fas fa-arrow-right"></i></span>
    </button>
    @endif
</div>
@endif